import datetime
from email.policy import default
from pydantic import BaseModel, Field
from pydantic import validator
from typing import Optional, List
from pydantic import BaseModel, ConfigDict, StringConstraints

from enum import Enum

from sqlalchemy import false


class LoginSerializer(BaseModel):
    email: str
    password: str


class CreateTrafficOfficerSerializer(BaseModel):
    name: str
    last_name: str
    nid: int
    phone: int
    email: str
    password: str


class Order(str, Enum):
    asc = "ASC"
    desc = "DESC"


class ParamGetSerializer(BaseModel):
    """."""

    id: Optional[int] = None
    nid: Optional[int] = None
    email: Optional[str] = None
    phone: Optional[int] = None
    active: bool = True


class ParamGetViolationsSerializer(BaseModel):
    """."""

    email: str


class TrafficOfficerDeserializer(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: int
    name: str
    last_name: str
    nid: int
    phone: int
    email: str
    active: bool
    created_at: Optional[datetime.datetime] = ""
    updated_at: Optional[datetime.datetime] = ""

    @validator("created_at")
    def cast_created_at(cls, created_at):
        return str(created_at)

    @validator("updated_at")
    def cast_updated_at(cls, updated_at):
        return str(updated_at)


class CreateTrafficViolationsSerializer(BaseModel):
    """."""

    description: str
    number_plate: str
    additional_data: Optional[dict] = {}
    violations_time: str


class ViolationsDeserializer(BaseModel):
    """."""

    model_config = ConfigDict(from_attributes=True)
    description: str
    number_plate: str
    violations_time: str
    traffic_officer_id: int
    active: bool
    violations_time: Optional[datetime.datetime] = ""
    created_at: Optional[datetime.datetime] = ""
    updated_at: Optional[datetime.datetime] = ""

    @validator("created_at")
    def cast_created_at(cls, created_at):
        return str(created_at)

    @validator("updated_at")
    def cast_updated_at(cls, updated_at):
        return str(updated_at)

    @validator("violations_time")
    def cast_violations_time(cls, violations_time):
        return str(violations_time)


class VehicleViolationsDeserializer(BaseModel):
    """."""

    model_config = ConfigDict(from_attributes=True)

    id: int
    brand: str
    model: str
    serial: str
    description: Optional[str]
    number_plate: str
    color: Optional[str] = None

    violations: Optional[List[ViolationsDeserializer]]

    active: bool
    created_at: Optional[datetime.datetime] = ""
    updated_at: Optional[datetime.datetime] = ""

    @validator("created_at")
    def cast_created_at(cls, created_at):
        return str(created_at)

    @validator("created_at")
    def cast_updated_at(cls, updated_at):
        return str(updated_at)
