from fastapi.testclient import TestClient
from config.main import app
import pytest

client = TestClient(app)


def test_create_driver():
    # Datos de prueba
    data = {
        "name": "Agente Test",
        "last_name": "Test 001",
        "nid": 1020304050,
        "phone": 400500600,
        "email": "agente1@mail.com",
        "password": "test123?",
    }

    response = client.post("/api/v1/officer", json=data)
    assert response.status_code == 200
